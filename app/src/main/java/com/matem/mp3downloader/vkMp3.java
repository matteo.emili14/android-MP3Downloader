package com.matem.mp3downloader;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by matteo on 12/11/17.
 */

public class vkMp3 extends AsyncTask<CharSequence, Integer, CharSequence> {

    ArrayList<Music> musicFoundList;

    private Context mContext;
    private View rootView;

    public vkMp3 (Context context, View rootView){
        this.mContext=context;
        this.rootView=rootView;
    }

    @Override
    protected CharSequence doInBackground(CharSequence... input) {

        String search = "http://vkmp3.org/mp3/"; //spazio è sostituito da %20

        String inputNormalized = input[0].toString().replaceAll("\\s","%20"); //sostituisco gli spazi

        Document doc = null;
        try {
            doc = Jsoup.connect(search+inputNormalized+"/").userAgent("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36").get();
        } catch (IOException e) {
            //non raggiungibile?
            e.printStackTrace();
            return input[0];
        }

        Elements elements = doc.select(".mp3");

        String title;
        String author;
        String time;
        String downloadLink;
        musicFoundList = new ArrayList<Music>();

        for(int i = 0; i < elements.size(); i++)
        {
            Element element = elements.get(i).child(4);
            //Log.d("DEBUG", element.toString());

            author = element.child(0).text().toString();

            if(element.child(1).className().equals("lyrics")) //se ha le liriche ci sta un child in più
            {
                title = element.child(3).text().toString();
                downloadLink = element.child(4).toString();
                time = element.child(5).text().toString();
            }
            else
            {
                title = element.child(2).text().toString();
                downloadLink = element.child(3).toString();
                time = element.child(4).text().toString();
            }

            downloadLink = downloadLink.substring(downloadLink.indexOf("http"), downloadLink.indexOf("');</script>"));

            publishProgress(i, elements.size());
            Log.d("DEBUG vkMp3", "Title: "+title+" Author: "+author+" Time: "+time+" Link: "+downloadLink);
            Music music = new Music(i, title, author, time, downloadLink, "vkMp3");

            musicFoundList.add(music);
        }

        return input[0];
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(Integer... values)
    {
        int progress = values[0];
        int total = values[1];

        int newProgress = (progress*30)/total; //progress:total = x:30 -> (progress*30)/total
        ProgressBar progressBar = rootView.findViewById(R.id.determinateBar);
        progressBar.setProgress(newProgress);
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(CharSequence input)
    {
        super.onPostExecute(input);

        ProgressBar progressBar = rootView.findViewById(R.id.determinateBar);
        progressBar.setProgress(30);

        new populateList().setMusicList(musicFoundList); //aggiorno musiche trovate
        new mp3Clan(mContext, rootView).execute(input);
    }

}
