package com.matem.mp3downloader;

/**
 * Created by matteo on 14/11/17.
 */

enum PlaybackStatus {
    PLAYING,
    PAUSED
}
