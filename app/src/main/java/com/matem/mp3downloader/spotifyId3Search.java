package com.matem.mp3downloader;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by matteo on 18/11/17.
 */

public class spotifyId3Search extends AsyncTask<String, Integer, String[]> {

    String clientId = "e0d39a60c6414e0ebb1e6fceff253b09";
    String clientSecret = "6270d2a72f7d4d7f9a9b29a649b9c415";

    String accessToken;
    Context context;


    public spotifyId3Search (Context context){
        this.context = context;
    }

    protected String[] doInBackground(String... input) {
        String[] album = null;

        initiateAuth();

        try {
            album = searchInfo(input[0], input[1]);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return album;
    }

    @Override
    protected void onPostExecute(String[] albumDetails) {
        /*
        String albumName = albumDetails[0];
        String albumCoverLink = albumDetails[1];
        */
        super.onPostExecute(albumDetails);

        albumInfoUpdate(albumDetails);

    }

    public void initiateAuth()
    {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("https://accounts.spotify.com/api/token");
        httppost.setHeader("Authorization", "Basic "+generateAuthorizationString());

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("grant_type", "client_credentials"));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            // Execute HTTP Post Request
            HttpResponse response = httpclient.execute(httppost);

            HttpEntity entity = response.getEntity();
            String responseString = EntityUtils.toString(entity);

            JSONObject obj = new JSONObject(responseString);
            accessToken = obj.getString("access_token");

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
        } catch (IOException e) {
            // TODO Auto-generated catch block
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String[] searchInfo(String title, String artist) throws IOException {

        title = URLEncoder.encode(title);
        artist = URLEncoder.encode(artist);

        String url = "https://api.spotify.com/v1/search?q=track:"+title+"%20artist:"+artist+"&type=track&limit=1";

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestProperty("Authorization", "Bearer "+accessToken);
        con.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null)
            response.append(inputLine);

        in.close();

        JSONObject jsonObject;

        String[] album = new String[2];

        try {
            jsonObject = new JSONObject(response.toString());

            album[0] = jsonObject.getJSONObject("tracks").getJSONArray("items").getJSONObject(0).getJSONObject("album").getString("name");
            album[1] = jsonObject.getJSONObject("tracks").getJSONArray("items").getJSONObject(0).getJSONObject("album").getJSONArray("images").getJSONObject(0).getString("url");
        } catch (JSONException e) {
            album[0] = null;
            album[1] = null;
            //e.printStackTrace();
        }

        return album;
    }


    public String generateAuthorizationString()
    {
        String string = clientId+":"+clientSecret;

        return new String(Base64.encodeBase64(string.getBytes()));
    }

    private void albumInfoUpdate(String[] album) {

        //recupera immagine album, la comprime e la converte per essere inviata all'activity principale

        Intent intent = new Intent("LOCAL_BROADCAST_UPDATE_ALBUM_INFO");
        intent.putExtra("INTENT_TYPE", "ALBUM_INFO");

        if(album[0] == null)
            intent.putExtra("FOUND", false);
        else
        {
            Bitmap bitmap = null;
            try {
                bitmap = BitmapFactory.decodeStream((InputStream)new URL(album[1]).getContent());
            } catch (IOException e) {
                e.printStackTrace();
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
            byte[] b = baos.toByteArray();

            intent.putExtra("FOUND", true);
            intent.putExtra("albumCover", b);
            intent.putExtra("albumCoverLink", album[1]);
            intent.putExtra("albumName", album[0]);
        }
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }
}
