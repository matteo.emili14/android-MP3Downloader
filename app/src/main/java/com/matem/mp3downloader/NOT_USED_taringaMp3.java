package com.matem.mp3downloader;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by matteo on 12/11/17.
 */

public class NOT_USED_taringaMp3 extends AsyncTask<CharSequence, Integer, CharSequence> {

    ArrayList<Music> musicFoundList;

    private Context mContext;
    private View rootView;

    public NOT_USED_taringaMp3(Context context, View rootView){
        this.mContext=context;
        this.rootView=rootView;
    }

    @Override
    protected CharSequence doInBackground(CharSequence... input) {

        String search = "http://taringamp3.com/musica/"; //spazio è sostituito da -

        String inputNormalized = input[0].toString().replaceAll("\\s","-"); //sostituisco gli spazi

        Document doc = null;
        try {
            doc = Jsoup.connect(search+inputNormalized).get();
        } catch (IOException e) {
            //non raggiungibile?
            e.printStackTrace();
        }

        Elements elements = doc.select(".cplayer-sound-item");

        String title;
        String author;
        String time;
        String downloadLink;
        musicFoundList = new ArrayList<Music>();

        for(int i = 0; i < elements.size(); i++)
        {
            Element element = elements.get(i).child(1).child(0);
            time = element.child(2).text().toString();
            title = element.child(1).text().toString();
            author = element.child(0).text().toString();
            downloadLink = "http:"+elements.get(i).attr("data-download-url").toString();

            Log.d("DEBUG TMP3", "Title: "+title+" Author: "+author+" Time: "+time+" Link: "+downloadLink);
            Music music = new Music(i, title, author, time, downloadLink, "taringaMP3");

            musicFoundList.add(music);
        }

        return input[0];
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(Integer... values)
    {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(CharSequence input)
    {
        super.onPostExecute(input);
        new populateList().setMusicList(musicFoundList); //aggiorno musiche trovate
        new mp3Clan(mContext, rootView).execute(input);
    }

}
