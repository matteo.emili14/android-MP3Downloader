package com.matem.mp3downloader;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

/**
 * Created by matteo on 19/11/17.
 */

public class settingsActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new settingsActivityFragment()).commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //TODO handle your request here

        if(data == null) //su alcuni dispositivi data ritorna null perchè?
            return;

        String str = data.getData().getPath();
        String folder = str.substring(str.lastIndexOf(":")+1);
        saveNewDownloadFolderLocation(folder);

        super.onActivityResult(requestCode, resultCode, data);
    }

    public static class settingsActivityFragment extends PreferenceFragment
    {
        @Override
        public void onCreate(final Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.settings_page);

            Preference myPref = findPreference("downloadFolder");
            myPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                    getActivity().startActivityForResult(intent, 1234);
                    return true;
                }
            });
        }
    }

    private void saveNewDownloadFolderLocation(String location)
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("downloadFolder", location);
        editor.apply();
    }
}
