package com.matem.mp3downloader;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by matteo on 13/11/17.
 */

public class mp3Clan extends AsyncTask<CharSequence, Integer, CharSequence> {

    ArrayList<Music> musicFoundList;
    private ListView musicListView;

    private Context mContext;
    private View rootView;

    public mp3Clan(Context context, View rootView){
        this.mContext=context;
        this.rootView=rootView;
    }

    Boolean mp3ClanDisabled = true;

    @Override
    protected CharSequence doInBackground(CharSequence... input) {

        if(mp3ClanDisabled)
            return input[0];

        String search = "http://mp3clan.it/mp3/"; //spazio è sostituito da _

        String inputNormalized = input[0].toString().replaceAll("\\s","_"); //sostituisco gli spazi

        Document doc = null;
        try {
            doc = Jsoup.connect(search+input[0]+".html").get();
        } catch (IOException e) {
            e.printStackTrace();
            return input[0];
        }

        Elements elements = doc.select(".mp3list-play");

        String title;
        String author;
        String time;
        String downloadLink;
        musicFoundList = new ArrayList<Music>();

        for(int i = 0; i < elements.size(); i++)
        {
            Element element = elements.get(i);

            //autore e titolo su questo sito sono insieme e divisi da un -
            String authorAndTitle = element.child(0).text().toString();
            Pattern pattern = Pattern.compile("- *");
            Matcher matcher = pattern.matcher(authorAndTitle);
            if (matcher.find()) {
                author = authorAndTitle.substring(0, matcher.start());
                title = authorAndTitle.substring(matcher.end());
            } else {
                author = "Sconosciuto";
                title = authorAndTitle;
            }

            time = element.child(2).text().toString().substring(6, 11); //Check 00:00 min
            downloadLink = element.child(1).attr("href").toString();

            //downloadLink = element.child(9).child(0).attr("href").toString();

            publishProgress(i, elements.size());
            Log.d("DEBUG mp3Clan", "Title: " + title + " Author: " + author + " Time: " + time + " Link: " + downloadLink);
            Music music = new Music(i, title, author, time, downloadLink, "mp3Clan");

            musicFoundList.add(music);
        }

        return input[0];
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(Integer... values)
    {
        int progress = values[0];
        int total = values[1];

        int newProgress = ((progress*30)/total) +30; //progress:total = x:30 -> (progress*30)/total +30 perchè è il secondo step
        ProgressBar progressBar = rootView.findViewById(R.id.determinateBar);
        progressBar.setProgress(newProgress);

        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(CharSequence input)
    {
        ProgressBar progressBar = rootView.findViewById(R.id.determinateBar);
        progressBar.setProgress(60);

        super.onPostExecute(input);
        ArrayList<Music> musicList = new populateList().getMusicList();
        new mergeAndOrder(mContext, rootView).execute(musicList, musicFoundList);
        //ultimo -> chiamo funzione riempimento lista
    }
}
