package com.matem.mp3downloader;


import android.util.Log;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;


/**
 * Created by matteo on 13/11/17.
 */

public class NOT_USED_m4Shared extends MainActivity {

    private static String consumer_key = "7002dc6d280ef9cd1b9119d4a945ec33";
    private static String consumer_secret = "c7117379c7b633bbf3eb68cd0272626fb91c6ee5";
    private static Base64 base64 = new Base64();
    private String oauth_token, oauth_token_secret;

    public void login() throws Exception {
        initiateAuth();
        //authorizeAuth();
        //accessAuth();
    }

    public void initiateAuth()
    {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("https://api.4shared.com/v1_2/oauth/initiate");

        try {
            String timestamp = String.valueOf((int)(System.currentTimeMillis() / 1000L));
            String uuid_string = UUID.randomUUID().toString();
            uuid_string = uuid_string.replaceAll("-", "");
            String oauth_nonce = uuid_string;
            String oauth_signature_method = "HMAC-SHA1";
            String parameter_string = URLEncoder.encode("oauth_consumer_key=" + consumer_key + "&oauth_nonce=" + oauth_nonce + "&oauth_signature_method=" + oauth_signature_method + "&oauth_timestamp=" + timestamp, "UTF-8");
            String url_string = URLEncoder.encode("https://api.4shared.com/v1_2/oauth/initiate", "UTF-8");

            String signature_base_string = "POST&"+url_string+"&"+parameter_string;
            //String oauth_signature = getSignature(url_string, parameter_string);
            String oauth_signature = computeSignature(signature_base_string, consumer_secret+"&");

            Log.d("OAUTH-1", "Base String:"+signature_base_string);
            Log.d("OAUTH-1", "Generated Signature:"+oauth_signature);

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("oauth_consumer_key", consumer_key));
            nameValuePairs.add(new BasicNameValuePair("oauth_signature_method", "HMAC-SHA1"));
            nameValuePairs.add(new BasicNameValuePair("oauth_timestamp", timestamp));
            nameValuePairs.add(new BasicNameValuePair("oauth_nonce", oauth_nonce));
            nameValuePairs.add(new BasicNameValuePair("oauth_signature", oauth_signature));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            // Execute HTTP Post Request
            HttpResponse response = httpclient.execute(httppost);

            HttpEntity entity = response.getEntity();
            String responseString = EntityUtils.toString(entity);

            oauth_token = responseString.substring(responseString.indexOf("oauth_token=")+"oauth_token=".length(), responseString.indexOf("&oauth_token_secret="));
            oauth_token_secret = responseString.substring(responseString.indexOf("&oauth_token_secret=")+"&oauth_token_secret=".length(), responseString.indexOf("&oauth_callback_confirmed="));

            //Log.d("OAUTH-1", responseString);
            Log.d("OAUTH-1", "oauth_token:"+oauth_token);
            Log.d("OAUTH-1", "oauth_token_secret:"+oauth_token_secret);

            /*String tokens[] = new String[2];

            tokens[0] = oauth_token;
            tokens[1] = oauth_token_secret;

            return tokens;*/

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
        } catch (IOException e) {
            // TODO Auto-generated catch block
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }

        //return null;
    }

    public void authorizeAuth() throws Exception {
        String url = "https://api.4shared.com/v1_2/oauth/authorize?oauth_token="+oauth_token;

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");

        int responseCode = con.getResponseCode();
        Log.d("OAUTH-2","Sending 'GET' request to URL : " + url);
        Log.d("OAUTH-2","Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null)
            response.append(inputLine);

        Log.d("OAUTH-2", response.toString());

        in.close();
    }

    public void accessAuth()
    {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("https://api.4shared.com/v1_2/oauth/token");

        try {
            String timestamp = String.valueOf((int)(System.currentTimeMillis() / 1000L));
            String uuid_string = UUID.randomUUID().toString();
            uuid_string = uuid_string.replaceAll("-", "");
            String oauth_nonce = uuid_string;
            String oauth_signature_method = "HMAC-SHA1";

            String parameter_string = URLEncoder.encode("oauth_consumer_key=" + consumer_key + "&oauth_nonce=" + oauth_nonce +
                    "&oauth_signature_method=" + oauth_signature_method + "&oauth_timestamp=" + timestamp +
                    "&oauth_token=" + oauth_token + "&oauth_token_secret=" + oauth_token_secret, "UTF-8");
            String url_string = URLEncoder.encode("https://api.4shared.com/v1_2/oauth/token", "UTF-8");

            String signature_base_string = "POST&"+url_string+"&"+parameter_string;
            String oauth_signature = computeSignature(signature_base_string, consumer_secret+"&");

            Log.d("OAUTH-3", signature_base_string);

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("oauth_consumer_key", consumer_key));
            nameValuePairs.add(new BasicNameValuePair("oauth_signature_method", "HMAC-SHA1"));
            nameValuePairs.add(new BasicNameValuePair("oauth_timestamp", timestamp));
            nameValuePairs.add(new BasicNameValuePair("oauth_nonce", oauth_nonce));
            nameValuePairs.add(new BasicNameValuePair("oauth_signature", oauth_signature));
            nameValuePairs.add(new BasicNameValuePair("oauth_token", oauth_token));
            nameValuePairs.add(new BasicNameValuePair("oauth_token_secret", oauth_token_secret));


            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            HttpResponse response = httpclient.execute(httppost);

            HttpEntity entity = response.getEntity();
            String responseString = EntityUtils.toString(entity);

            //oauth_token = responseString.substring(responseString.indexOf("oauth_token=")+"oauth_token=".length(), responseString.indexOf("&oauth_token_secret="));
            //oauth_token_secret = responseString.substring(responseString.indexOf("&oauth_token_secret=")+"&oauth_token_secret=".length(), responseString.indexOf("&oauth_callback_confirmed="));


            Log.d("OAUTH-3", responseString);
            /*Log.d("OAUTH-3", "oauth_token:"+oauth_token);
            Log.d("OAUTH-3", "oauth_token_secret:"+oauth_token_secret);*/

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }


    }

    private static String computeSignature(String baseString, String keyString) throws GeneralSecurityException, UnsupportedEncodingException {

        SecretKey secretKey = null;

        byte[] keyBytes = keyString.getBytes();
        secretKey = new SecretKeySpec(keyBytes, "HmacSHA1");

        Mac mac = Mac.getInstance("HmacSHA1");
        mac.init(secretKey);

        byte[] text = baseString.getBytes();

        return new String(Base64.encodeBase64(mac.doFinal(text))).trim();
    }




}
