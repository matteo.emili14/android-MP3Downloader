package com.matem.mp3downloader;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by matteo on 14/11/17.
 */

public class ListAdapter extends ArrayAdapter<Music> {

    private View rootView;

    public ListAdapter(Context context, int resource, List<Music> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.music_list, null);
        }

        //torna null?
        ArrayList<Music> musicList = new populateList().getMusicList();
        Music music;

        if(musicList == null)
            music = getItem(position);
        else
            music = musicList.get(position);

        if (music != null) {
            TextView tt1 = (TextView) v.findViewById(R.id.MusicTitle);
            TextView tt2 = (TextView) v.findViewById(R.id.MusicAuthor);
            ImageView img = (ImageView) v.findViewById(R.id.albumCover);

            img.setVisibility(View.INVISIBLE);

            StorageUtil storage = new StorageUtil(getContext());
            int audioIndex = storage.loadAudioIndex();

            //position è la posizione dell'ultimo elemento renderizzato -> se scrollo verso il basso l'ultimo della lista altrimenti se scrollo verso l'altro è il primo
            if (tt1 != null) {
                tt1.setText(music.getTitle());
            }

            if (tt2 != null) {
                tt2.setText(music.getAuthor());
            }

            if(audioIndex == position)
            {
                img.setVisibility(View.VISIBLE);
                //visibile
            }
        }

        return v;
    }

}
