package com.matem.mp3downloader;

import android.app.ListActivity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;

/**
 * Created by matteo on 13/11/17.
 */

class mergeAndOrder extends AsyncTask<ArrayList<Music>, Integer, ArrayList<Music>>
{

    private Context mContext;
    private View rootView;

    public mergeAndOrder(Context context, View rootView){
        this.mContext=context;
        this.rootView=rootView;
    }

    @Override
    protected ArrayList<Music> doInBackground(ArrayList<Music>... arrayLists)
    {
        ArrayList<Music> musicList1 = arrayLists[0];
        ArrayList<Music> musicList2 = arrayLists[1];

        if(musicList1 == null || musicList1.size() == 0)
            return musicList2;
        else if(musicList2 == null || musicList2.size() == 0)
            return musicList1;

        ArrayList<Music> orderedMusicList = new ArrayList<>();
        ArrayList<Music> shortestList, longestList;

        int counter1 = 0;
        int counter2 = 0;

        if(musicList1.size() < musicList2.size()) {
            shortestList = musicList1; //counter1
            longestList = musicList2; //counter2
        }
        else {
            shortestList = musicList2;
            longestList = musicList1;
        }

        for(int i = 0; i < shortestList.size(); i++)
        {
            if(shortestList.get(counter1).getId() < longestList.get(counter2).getId())
            {
                orderedMusicList.add(shortestList.get(counter1));
                counter1++;
            }
            else
            {
                orderedMusicList.add(longestList.get(counter2));
                counter2++;
            }
        }

        while(counter2 < longestList.size())
        {
            orderedMusicList.add(longestList.get(counter2));
            counter2++;
        }

        return orderedMusicList;
    }

    protected void onPostExecute(ArrayList<Music> musicList)
    {
        ProgressBar progressBar = rootView.findViewById(R.id.determinateBar);
        progressBar.setProgress(80);

        new populateList().setMusicList(musicList); //imposto musicList ordinato e unito
        new populateList().populateList(mContext, rootView);

        new StorageUtil(mContext).clearCachedAudioPlaylist();
        new StorageUtil(mContext).storeAudio(musicList);

        super.onPostExecute(musicList);
    }
}

public class populateList extends ListActivity
{

    public static ArrayList<Music> musicList;

    public int getAudioIndex() {
        return audioIndex;
    }

    public void setAudioIndex(int audioIndex) {
        this.audioIndex = audioIndex;
    }

    public int audioIndex;

    public ArrayList<Music> getMusicList() {
        return musicList;
    }

    public void setMusicList(ArrayList<Music> musicList) {
        this.musicList = musicList;
    }


    public void populateList(Context context, View rootView)
    {
        ArrayList<Music> musicArrayList = new populateList().getMusicList();

        if(musicArrayList == null)
            return; //nessun risultato

        ProgressBar progressBar = rootView.findViewById(R.id.determinateBar);
        progressBar.setProgress(100);

        //progressBar.setVisibility(ProgressBar.INVISIBLE);
        progressBar.setProgress(0);

        ListAdapter customAdapter = new ListAdapter(context, R.layout.music_list, musicArrayList);
        new populateList().setMusicList(musicArrayList);
        ListView musicListView = (ListView) rootView.findViewById(R.id.musicList);
        //customAdapter.clear(); //clear list
        musicListView.setAdapter(customAdapter); //fill list
    }


    /*public Boolean compare(Music m1, Music m2) {
        if(m1.getId() < m2.getId())
            return true;
        else
            return false;
    }*/

}
