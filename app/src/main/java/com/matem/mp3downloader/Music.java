package com.matem.mp3downloader;

/**
 * Created by matteo on 12/11/17.
 */

public class Music {

    public Music(int id, String title, String author, String time, String downloadLink, String source) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.album = "Sconosciuto";
        this.time = time;
        this.quality = null;
        this.albumCover = null;
        this.downloadLink = downloadLink;
        this.source = source;
    }

    public Music(int id, String title, String author, String album, String time, String quality, String albumCover, String downloadLink, String source) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.time = time;
        this.quality = quality;
        this.albumCover = albumCover;
        this.downloadLink = downloadLink;
        this.source = source;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public String getAlbumCover() {
        return albumCover;
    }

    public void setAlbumCover(String albumCover) {
        this.albumCover = albumCover;
    }

    public String getDownloadLink() {
        return downloadLink;
    }

    public void setDownloadLink(String downloadLink) {
        this.downloadLink = downloadLink;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    @Override
    public String toString() {
        return "Music{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", time='" + time + '\'' +
                ", quality='" + quality + '\'' +
                ", albumCover='" + albumCover + '\'' +
                ", downloadLink='" + downloadLink + '\'' +
                ", source='" + source + '\'' +
                '}';
    }

    public String title;

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    public int id;
    public String album;
    public String author;
    public String time;
    public String quality;
    public String albumCover;
    public String downloadLink;
    public String source;


}
