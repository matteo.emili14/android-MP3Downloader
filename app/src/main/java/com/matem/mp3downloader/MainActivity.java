package com.matem.mp3downloader;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    //player
    private MediaPlayerService player;
    boolean serviceBound = false;
    static boolean playing = false;
    boolean newPlaylist = false;
    public static final String Broadcast_PLAY_NEW_AUDIO = "com.matem6.mp3downloader.PlayNewAudio";
    public static final String Broadcast_PLAY_NEW_PLAYLIST = "com.matem6.mp3downloader.PlayNewPlaylist";
    public static final String Broadcast_PLAY_PAUSE = "com.matem6.mp3downloader.PlayPausePlayer";
    public static final String Broadcast_SKIP_NEXT = "com.matem6.mp3downloader.SkipNext";
    public static final String Broadcast_SKIP_PREVIOUS = "com.matem6.mp3downloader.SkipPrevious";
    public static final String Broadcast_SEEK_TO = "com.matem6.mp3downloader.SeekTo";
    public static final String Broadcast_UPDATE_ALBUM_INFO = "com.matem6.mp3downloader.UpdateAlbumInfo";

    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 65;

    static String defaultDownloadFolder = Environment.DIRECTORY_DOWNLOADS;

    static ListAdapter customAdapter;
    static ListView musicListView;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //chiede permessi all'avvio
        if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
        }

        setContentView(R.layout.activity_main);

        //necessari per eseguire azioni in rete da classi esterne (vedi NOT_USED_taringaMp3)
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        LocalBroadcastManager.getInstance(this).registerReceiver(register_updateSeekBar, new IntentFilter("LOCAL_BROADCAST_UPDATE_SEEKBAR"));
        LocalBroadcastManager.getInstance(this).registerReceiver(register_updateSeekBarLength, new IntentFilter("LOCAL_BROADCAST_UPDATE_SEEKBAR_LENGTH"));
        LocalBroadcastManager.getInstance(this).registerReceiver(register_resetSeekBarLength, new IntentFilter("LOCAL_BROADCAST_RESET_SEEKBAR"));
        LocalBroadcastManager.getInstance(this).registerReceiver(register_updatePlayerInfo, new IntentFilter("LOCAL_BROADCAST_UPDATE_PLAYER_INFO"));
        LocalBroadcastManager.getInstance(this).registerReceiver(register_onPause, new IntentFilter("LOCAL_BROADCAST_ON_PAUSE"));
        LocalBroadcastManager.getInstance(this).registerReceiver(register_updateAlbumInfo, new IntentFilter("LOCAL_BROADCAST_UPDATE_ALBUM_INFO"));
        LocalBroadcastManager.getInstance(this).registerReceiver(register_onPlayStatusChange, new IntentFilter("LOCAL_BROADCAST_ON_PLAY_STATUS_CHANGE"));
        LocalBroadcastManager.getInstance(this).registerReceiver(register_updateBufferingState, new IntentFilter("LOCAL_BROADCAST_UPDATE_BUFFERING"));
        LocalBroadcastManager.getInstance(this).registerReceiver(register_sendMessage, new IntentFilter("LOCAL_BROADCAST_SEND_MESSAGE"));


        //Download manager
        registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

        final ImageButton playButton = findViewById(R.id.playButton);
        final ImageView miniPlayButton = findViewById(R.id.playButtonMinimized);

        final ImageButton nextButton = findViewById(R.id.nextButton);
        final ImageButton previousButton = findViewById(R.id.previousButton);

        final SeekBar seekBar = findViewById(R.id.seekbar);

        miniPlayButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(player != null) //già avviato una volta
                {
                    sendBroadcast(new Intent(Broadcast_PLAY_PAUSE)); //gestisce play e ripresa audio

                    //gestito in MediaPlayerService con local receiver
                    /*if(playing) { //se sta riproducendo metti in pausa e cambia icona con triangolo
                        playButton.setImageResource(R.drawable.ic_play_arrow_black_24dp);
                        miniPlayButton.setImageResource(R.drawable.ic_play_arrow_black_24dp);
                        playing = false;
                    }
                    else { //se sta in pausa metti in riproduzione e cambia icona con rettangolo
                        playButton.setImageResource(R.drawable.ic_pause_black_24dp);
                        miniPlayButton.setImageResource(R.drawable.ic_pause_black_24dp);
                        playing = true;
                    }*/
                }
            }
        });

        playButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(player != null) //già avviato una volta
                {
                    sendBroadcast(new Intent(Broadcast_PLAY_PAUSE)); //gestisce play e ripresa audio


                    //gestito in MediaPlayerService con local receiver
                    /*if(playing) { //se sta riproducendo metti in pausa e cambia icona con triangolo
                        playButton.setImageResource(R.drawable.ic_play_arrow_black_24dp);
                        miniPlayButton.setImageResource(R.drawable.ic_play_arrow_black_24dp);
                        playing = false;
                    }
                    else { //se sta in pausa metti in riproduzione e cambia icona con rettangolo
                        playButton.setImageResource(R.drawable.ic_pause_black_24dp);
                        miniPlayButton.setImageResource(R.drawable.ic_pause_black_24dp);
                        playing = true;
                    }*/
                }
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(player != null) //già avviato una volta
                {
                    sendBroadcast(new Intent(Broadcast_SKIP_NEXT)); //gestisce play e ripresa audio
                }
            }
        });

        previousButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(player != null) //già avviato una volta
                {
                    sendBroadcast(new Intent(Broadcast_SKIP_PREVIOUS)); //gestisce play e ripresa audio
                }
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int newProgress = 0;
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser)
                {
                    newProgress = progress;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Intent seekToIntent = new Intent(Broadcast_SEEK_TO);
                seekToIntent.putExtra("INTENT_TYPE", "SEEKBAR_RESULT");
                seekToIntent.putExtra("NEW_POSITION", newProgress);

                sendBroadcast(seekToIntent); //vai a minuto
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        SlidingUpPanelLayout layout = findViewById(R.id.sliding_layout);

        if(layout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED)
            layout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        else
            finish();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId()==R.id.musicList) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_list, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch(item.getItemId()) {
            case R.id.downloadPopupMenu:
                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
                String downloadLink = new populateList().getMusicList().get((int)info.id).downloadLink;
                String downloadFolderPath = sharedPref.getString("downloadFolder", defaultDownloadFolder);

                startDownload(downloadLink, downloadFolderPath);

                return true;
            /*case R.id.edit:
                // edit stuff here
                return true;
            case R.id.delete:
                // remove stuff here
                return true;*/
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void startDownload(String downloadLink, String downloadFolderPath) {

        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(downloadLink));
        //request.setDescription("Some descrition");

        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

        String fileName = URLUtil.guessFileName(downloadLink, null, null);
        request.setTitle(fileName);

        request.setDestinationInExternalPublicDir(downloadFolderPath, fileName);

        // get download service and enqueue file
        DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);

        if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
        {
            manager.enqueue(request);
            //File write logic here
        }
        else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
        }
        /*if(ContextCompat.checkSelfPermission(this, Manifest.permission.) == PackageManager.PERMISSION_GRANTED)
        else
            requestPermissions(this, new String[]{Manifest.permission.}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);*/

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem menuSearch = menu.findItem(R.id.action_search);
        final SearchView search = (SearchView) MenuItemCompat.getActionView(menuSearch);


        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query != null) {
                    startSearch(search.getQuery()); //start search on engines
                }
                search.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //quando cambia il testo
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(this, settingsActivity.class); //apre impostazioni
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            View rootView;
            rootView = inflater.inflate(R.layout.activity_main, container, false);

            if(getArguments().getInt(ARG_SECTION_NUMBER) == 1)
            {
                ArrayList<Music> musicArrayList = new ArrayList<>();
                rootView = inflater.inflate(R.layout.fragment_page1_search, container, false);

                customAdapter = new ListAdapter(getContext(), R.layout.music_list, musicArrayList);

                musicListView = (ListView) rootView.findViewById(R.id.musicList);
                registerForContextMenu(musicListView); //menu pressione prolungata
                musicListView.setAdapter(customAdapter);

                final ImageButton playButton = rootView.findViewById(R.id.playButton);

                musicListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        int musicId = (int) id;
                        ArrayList<Music> musicArrayList = new populateList().getMusicList();
                        String downloadLink = musicArrayList.get(musicId).getDownloadLink();

                        ((MainActivity)getActivity()).playAudio(musicId);
                        //funzione avvio riproduzione
                    }
                });

                return rootView;
            }
            else if(getArguments().getInt(ARG_SECTION_NUMBER) == 2)
            {
                //fai qualcosa tab 2
                rootView = inflater.inflate(R.layout.fragment_page2_downloaded, container, false);

                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
                String downloadFolderPath = sharedPref.getString("downloadFolder", defaultDownloadFolder);

                String path = Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+downloadFolderPath;
                File directory = new File(path);
                File[] files = directory.listFiles();

                if(files != null) {
                    String[] theNamesOfFiles = new String[files.length];

                    for (int i = 0; i < theNamesOfFiles.length; i++) {
                        theNamesOfFiles[i] = files[i].getName();
                    }

                    ArrayAdapter adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, theNamesOfFiles);

                    ListView downloadedListView = (ListView) rootView.findViewById(R.id.downloadedList);
                    registerForContextMenu(downloadedListView); //menu pressione prolungata
                    downloadedListView.setAdapter(adapter);
                }

                return rootView;
            }

            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }
    }

    public void startSearch(CharSequence input)
    {
        //vkmp3 search
        //avvio ricerca e imposto progressbar visibile
        ProgressBar progressBar = findViewById(R.id.determinateBar);

        //progressBar.setVisibility(ProgressBar.VISIBLE);

        progressBar.setProgress(0);

        new vkMp3(this, findViewById(android.R.id.content)).execute(input);
        newPlaylist = true;
    }

    //Binding this Client to the AudioPlayer Service
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            MediaPlayerService.LocalBinder binder = (MediaPlayerService.LocalBinder) service;
            player = binder.getService();
            serviceBound = true;

            //Toast.makeText(MainActivity.this, "Service Bound", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            serviceBound = false;
        }
    };

    private void playAudio(int audioIndex) {
        //Check is service is active
        if (!serviceBound) {
            //Store Serializable audioList to SharedPreferences
            StorageUtil storage = new StorageUtil(getApplicationContext());
            storage.storeAudio(new populateList().getMusicList()); //--> moved into mergeandorder method
            storage.storeAudioIndex(audioIndex);

            Intent playerIntent = new Intent(this, MediaPlayerService.class);
            startService(playerIntent);
            bindService(playerIntent, serviceConnection, Context.BIND_AUTO_CREATE);
        } else {
            //Store the new audioIndex to SharedPreferences
            StorageUtil storage = new StorageUtil(getApplicationContext());
            storage.storeAudioIndex(audioIndex);

            //Service is active
            //Send a broadcast to the service -> PLAY_NEW_AUDIO
            Intent broadcastIntent;
            if(newPlaylist)
            {
                broadcastIntent = new Intent(Broadcast_PLAY_NEW_PLAYLIST);
                newPlaylist = false;
            }
            else
                broadcastIntent = new Intent(Broadcast_PLAY_NEW_AUDIO);

            sendBroadcast(broadcastIntent);
        }

      playing = true;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean("ServiceState", serviceBound);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        serviceBound = savedInstanceState.getBoolean("ServiceState");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(register_updateSeekBar);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(register_updateSeekBarLength);


        if (serviceBound) {
            unbindService(serviceConnection);
            //service is active
            player.stopSelf();
        }
    }

    BroadcastReceiver onComplete = new BroadcastReceiver() {
        public void onReceive(Context ctxt, Intent intent) {
            //download completato
        }
    };


    private BroadcastReceiver register_updateSeekBar = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String intentType = intent.getStringExtra("INTENT_TYPE");
            if(intentType.equalsIgnoreCase("SEEKBAR_RESULT"))
            {
                int currPosition = intent.getIntExtra("CURRENT_POSITION", 0);
                SeekBar seekBar = findViewById(R.id.seekbar);
                seekBar.setProgress(currPosition);
                //aggiorna seekbar
            }
        }
    };

    private BroadcastReceiver register_updateSeekBarLength = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String intentType = intent.getStringExtra("INTENT_TYPE");
            if(intentType.equalsIgnoreCase("SEEKBAR_RESULT"))
            {
                int length = intent.getIntExtra("LENGTH", 0);
                SeekBar seekBar = findViewById(R.id.seekbar);
                seekBar.setMax(length);
                //aggiorna seekbar
            }
        }
    };

    private BroadcastReceiver register_resetSeekBarLength = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String intentType = intent.getStringExtra("INTENT_TYPE");
            if(intentType.equalsIgnoreCase("SEEKBAR_RESULT"))
            {
                SeekBar seekBar = findViewById(R.id.seekbar);
                seekBar.setProgress(0);
                seekBar.setMax(0);
                //aggiorna seekbar
            }
        }
    };

    private BroadcastReceiver register_updateBufferingState = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String intentType = intent.getStringExtra("INTENT_TYPE");
            if(intentType.equalsIgnoreCase("BUFFERING"))
            {
                int progress = intent.getIntExtra("CURRENT_POSITION",0);
                ProgressBar bufferingProgressBar1 = findViewById(R.id.determinateBar);
                ProgressBar bufferingProgressBar2 = findViewById(R.id.determinateBar2);
                bufferingProgressBar1.setSecondaryProgress(progress);
                bufferingProgressBar2.setProgress(progress);
                //aggiorna seekbar
            }
        }
    };

    private BroadcastReceiver register_sendMessage = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String intentType = intent.getStringExtra("INTENT_TYPE");
            if(intentType.equalsIgnoreCase("MESSAGE"))
            {
                String message = intent.getStringExtra("message");
                Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
            }
        }
    };



    private BroadcastReceiver register_updatePlayerInfo = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            musicListView.invalidateViews(); //renderizza di nuovo listview

            String intentType = intent.getStringExtra("INTENT_TYPE");

            if(intentType.equalsIgnoreCase("PLAYER_INFO")) {
                TextView titlePlaying = findViewById(R.id.musicTitle);
                TextView authorPlaying = findViewById(R.id.musicAuthor);

                int audioIndex = intent.getIntExtra("AUDIO_INDEX", 0);

                //imposto titolo e autore nella barra del player
                ArrayList<Music> musicList = new populateList().getMusicList();

                String title = musicList.get(audioIndex).getTitle();
                String artist = musicList.get(audioIndex).getAuthor();
                titlePlaying.setText(title);
                authorPlaying.setText(artist);
                //TODO impostare cover album -> recuperare ID3

                playing = true;
                ImageButton playButton = findViewById(R.id.playButton);
                ImageView miniPlayButton = findViewById(R.id.playButtonMinimized);
                playButton.setImageResource(R.drawable.ic_pause_black_24dp);
                miniPlayButton.setImageResource(R.drawable.ic_pause_black_24dp);
            }
        }
    };



    private BroadcastReceiver register_onPlayStatusChange = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String intentType = intent.getStringExtra("INTENT_TYPE");

            if(intentType.equalsIgnoreCase("PLAYING_STATUS")) {
                int index = intent.getIntExtra("INDEX", 0);
                Boolean play = intent.getBooleanExtra("PLAY", true);

                /*ListView listMusic = findViewById(R.id.musicList);

                ImageView playingMark = (ImageView) listMusic.getTag(index);

                if(play)
                    playingMark.setVisibility(View.VISIBLE);
                else
                    playingMark.setVisibility(View.INVISIBLE);*/
            }
        }
    };

    private BroadcastReceiver register_onPause = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String intentType = intent.getStringExtra("INTENT_TYPE");

            if(intentType.equalsIgnoreCase("PLAYER_INFO")) {
                Boolean isPlayAction = intent.getBooleanExtra("ACTION", false);

                ImageButton playButton = findViewById(R.id.playButton);
                ImageView miniPlayButton = findViewById(R.id.playButtonMinimized);

                if(isPlayAction)
                {
                    playButton.setImageResource(R.drawable.ic_pause_black_24dp);
                    miniPlayButton.setImageResource(R.drawable.ic_pause_black_24dp);
                    playing = true;
                }
                else
                {
                    playButton.setImageResource(R.drawable.ic_play_arrow_black_24dp);
                    miniPlayButton.setImageResource(R.drawable.ic_play_arrow_black_24dp);
                    playing = false;
                }
            }
        }
    };

    private BroadcastReceiver register_updateAlbumInfo = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String intentType = intent.getStringExtra("INTENT_TYPE");
            if(intentType.equalsIgnoreCase("ALBUM_INFO"))
            {
                Intent albumCoverIntent = new Intent(Broadcast_UPDATE_ALBUM_INFO);

                ImageView albumCoverView = findViewById(R.id.bigAlbumCover);
                ImageView miniAlbumCoverView = findViewById(R.id.miniAlbumCover);

                if(!intent.getBooleanExtra("FOUND", true))
                {
                    miniAlbumCoverView.setImageResource(R.drawable.no_cover);
                    albumCoverView.setImageResource(R.drawable.no_cover);

                    albumCoverIntent.putExtra("INTENT_TYPE", "ALBUM_INFO");
                    albumCoverIntent.putExtra("FOUND", false);
                }
                else {

                    byte[] b = intent.getByteArrayExtra("albumCover");
                    Bitmap bmp = BitmapFactory.decodeByteArray(b, 0, b.length);

                    miniAlbumCoverView.setImageBitmap(bmp);
                    albumCoverView.setImageBitmap(bmp);

                    String albumName = intent.getStringExtra("albumName");
                    String albumImageLink = intent.getStringExtra("albumCoverLink");

                    albumCoverIntent.putExtra("INTENT_TYPE", "ALBUM_INFO");
                    albumCoverIntent.putExtra("FOUND", true);
                    albumCoverIntent.putExtra("albumCoverLink", albumImageLink);
                    albumCoverIntent.putExtra("albumName", albumName);
                }

                sendBroadcast(albumCoverIntent);
            }
        }
    };
}
